<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/home', function () {
    return view('welcome');
});*/

/*Route::get('/tes/{tester}', function($tester){
    return "cuma halaman tester $tester";
});*/

/*Route::get('/master', function(){
 return view('layouts.master');
});*/

/*Route::get('/index', function(){
    return view('items.index');
   });*/

   /*Route::get('/index/create', function(){
    return view('items.create');
   });*/

   Route::get('/', function(){
    return view('items.table');
   });

   Route::get('/data-tables', function(){
    return view('items.datatables');
   });

/*Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});

Route::get('train/{angka}', function ($angka) {
    return view('train', ["angka"=>$angka]);
});*/